﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AfterClass.CoreModels.Interfaces
{
    public enum RoleType
    {
        Administrator = 0,
        Teacher = 1,
        Student = 2
    }

    public interface IUserEntity
    {
        string UserName
        {
            get; set;
        }

        string Password
        {
            get;set;
        }

        string Email
        {
            get;set;
        }

        RoleType Role
        {
            get;set;
        }
    }
}
