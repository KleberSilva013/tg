﻿using FluentNHibernate.Mapping;

namespace AfterClass.CoreModels.Entities
{
    public class BaseEntity
    {
        public virtual int Id
        {
            get; set;
        }

        public virtual int Version
        {
            get; set;
        }

        public class BaseEntityMap<T> : ClassMap<T> where T : BaseEntity
        {
            public BaseEntityMap(string sequenceName)
            {
                Id(x => x.Id).GeneratedBy.Native(sequenceName);

                Version(x => x.Version);
            }
        }
    }
}
