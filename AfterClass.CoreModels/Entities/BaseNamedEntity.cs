﻿using AfterClass.CoreModels.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AfterClass.CoreModels.Entities
{
    public class BaseNamedEntity : BaseEntity
    {
        private string mName;

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Nome não deve estar vazio", AllowEmptyStrings = false)]
        [MinLength(3, ErrorMessage = "Nome não deve ser muito curto")]
        [MaxLength(30, ErrorMessage = "Nome não deve ter mais de 30 caracteres")]
        public virtual string Name
        {
            get
            {
                return mName;
            }
            set
            {
                if(value.Where(x => char.IsNumber(x)).Any())
                    throw new ArgumentException(nameof(Name), "Nome não deve conter números");

                mName = value;
            }
        }

        public class BaseNamedEntityMap<T> : BaseEntityMap<T> where T : BaseNamedEntity
        {
            public BaseNamedEntityMap(int lenght, string sequenceName) : base(sequenceName)
            {
                Map(x => x.Name)
                    .Not.Nullable()
                    .Length(lenght);
            }
        }
    }
}
