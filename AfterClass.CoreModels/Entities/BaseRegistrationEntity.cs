﻿using AfterClass.CoreModels.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AfterClass.CoreModels.Entities
{
    public class BaseRegistrationEntity : BaseNamedEntity, IUserEntity
    {
        private string mUserName;

        [Display(Name = "Nome de Usuário")]
        [Required(ErrorMessage = "Nome de usuário não pode estar vazio", AllowEmptyStrings = false)]
        [MinLength(4, ErrorMessage = "Nome de usuário não pode ser muito curto.")]
        [MaxLength(15, ErrorMessage = "Nome de usuário não deve ultrapassar 15 letras")]
        public virtual string UserName
        {
            get
            {
                return mUserName;
            }
            set
            {
                if(value.Where(x => char.IsWhiteSpace(x) || char.IsSeparator(x) || char.IsSymbol(x)).Any())
                    throw new ArgumentException(nameof(UserName), "Nome de usuário não deve conter espaços e symbolos");

                mUserName = value;
            }
        }

        private string mPassword;

        [Display(Name = "Senha")]
        [Required(ErrorMessage = "Senha não deve estar vazia", AllowEmptyStrings = false)]
        public virtual string Password
        {
            get
            {
                return mPassword;
            }
            set
            {
                mPassword = value;
            }
        }

        [Display(Name = "Permissão")]
        [Required]
        public virtual RoleType Role
        {
            set;get;
        }

        [Required(ErrorMessage = "Email não deve estar vazio")]
        [EmailAddress(ErrorMessage = "Informe um email válido")]
        public virtual string Email
        {
            get;set;
        }

        public class BaseRegistrationEntityMap<T> : BaseNamedEntityMap<T> where T : BaseRegistrationEntity
        {
            public BaseRegistrationEntityMap(int lenght, string sequenceName) : base(lenght, sequenceName)
            {
                Map(x => x.UserName)
                    .Not.Nullable()
                    .Length(15);

                Map(x => x.Password)
                    .Not.Nullable();

                Map(x => x.Email)
                    .Not.Nullable();

                Map(x => x.Role)
                    .Not.Nullable();
            }
        }
    }
}
