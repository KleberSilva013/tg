﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AfterClass.CoreModels.ViewModels
{
    public class BaseNamedViewModel
    {
        public int Id
        {
            get; set;
        }

        private string mName;

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Nome não deve estar vazio", AllowEmptyStrings = false)]
        [MinLength(3, ErrorMessage = "Nome não deve ser muito curto")]
        [MaxLength(30, ErrorMessage = "Nome não deve ter mais de 30 caracteres")]
        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                if(value.Where(x => char.IsNumber(x)).Any())
                    throw new ArgumentException(nameof(Name), "Nome não deve possuir numeros");
                mName = value;
            }
        }
    }
}
