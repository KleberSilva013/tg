﻿using AfterClass.CoreModels.Entities;
using AfterClass.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AfterClass.Models
{
    public class Semester : BaseEntity
    {
        private int mYear;

        [Display(Name = "Ano")]
        public virtual int Year
        {
            get
            {
                return mYear;
            }
            set
            {
                if(mYear < 1900 || mYear > DateTime.Now.Year + 1)
                    throw new InvalidYearArgumentException("O ano não deve ser menor que 1900 e maior que " + DateTime.Now.Year + 1);

                mYear = value;
            }
        }


        private int mStage;

        public virtual int Stage
        {
            get
            {
                return mStage;
            }
            set
            {
                mStage = value;
            }
        }

        private IList<Course> mCourses;

        protected virtual IList<Course> Courses
        {
            get
            {
                return mCourses;
            }
            set
            {
                mCourses = value;
            }
        }

        public virtual void AddCourse(Course course)
        {
            if(course == null)
                throw new NullCourseException("Impossível adicionar curso inválido");

            if(mCourses.Where(x => x.Id == course.Id).Any())
                throw new System.InvalidOperationException("O curso ja faz parte deste semestre");

            course.Semester = this;

            mCourses.Add(course);
        }

        public virtual void RemoveCourse(Course course)
        {
            if(course == null)
                throw new NullCourseException("Impossível remover curso inválido");

            var cour = mCourses.Where(x => x.Id == course.Id).FirstOrDefault()
                ?? throw new System.InvalidOperationException("O curso não faz parte deste semestre");

            mCourses.Remove(cour);
        }

        public class SemesterMap : BaseEntityMap<Semester>
        {
            public SemesterMap() : base("semester_id")
            {
                Map(x => x.Year)
                    .Not.Nullable();

                Map(x => x.Stage)
                    .Not.Nullable();

                HasMany(x => x.Courses)
                    .Inverse()
                    .KeyColumn("course_id")
                    .LazyLoad();
            }
        }
    }
}