﻿using AfterClass.CoreModels.Entities;
using AfterClass.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace AfterClass.Models
{
    public class Discipline : BaseNamedEntity
    {
        public virtual Teacher Teacher
        {
            get;set;
        }

        private IList<Student> mStudents = new List<Student>();

        protected virtual IList<Student> Students
        {
            get
            {
                return mStudents;
            }
            set
            {
                mStudents = value;
            }
        }

        public virtual Student[] GetStudents()
        {
            return mStudents.ToArray();
        }

        public virtual void UpdateStudents(Student[] students)
        {
            var keep = mStudents.Where(x => students.Where(y => x.Id == y.Id).FirstOrDefault() != null).ToArray();

            foreach(var s in mStudents)
            {
                bool ok = true;
                foreach(var ks in keep)
                {
                    if(ks.Id != s.Id)
                        ok = false;
                }
                if(!ok)
                    mStudents.Remove(s);
            }

            foreach(var s in students)
            {
                bool ok = true;
                foreach(var ks in keep)
                {
                    if(ks.Id != s.Id)
                        ok = false;
                }
                if(!ok)
                    mStudents.Add(s);
            }
        }

        public virtual void AddStudent(Student student)
        {
            if (student == null)
                throw new NullStudentException("Impossível adicionar aluno inválido");

            if (mStudents.Where(x => x.Id == student.Id).Any())
                throw new System.InvalidOperationException("O aluno ja faz parte desta discplina");

            mStudents.Add(student);
        }

        public virtual void RemoveStudent(Student student)
        {
            if (student == null)
                throw new NullCourseException("Impossível remover aluno inválido");

            var stud = mStudents.Where(x => x.Id == student.Id).FirstOrDefault()
                ?? throw new System.InvalidOperationException("O aluno não faz parte desta disciplina");

            mStudents.Remove(stud);
        }

        public class DiscplineMap : BaseNamedEntityMap<Discipline>
        {
            public DiscplineMap() : base(30, "discipline_id")
            {
                References(x => x.Teacher);

                HasManyToMany(x => x.Students)
                    .ChildKeyColumn("student_id")
                    .ParentKeyColumn("discipline_id")
                    .Cascade.AllDeleteOrphan();
            }
        }
    }
}