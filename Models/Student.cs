﻿using AfterClass.CoreModels.Entities;

namespace AfterClass.Models
{
    public class Student : BaseRegistrationEntity
    {
        public Student()
        {
            Role = CoreModels.Interfaces.RoleType.Student;
        }

        public class StudentMap : BaseRegistrationEntityMap<Student>
        {
            public StudentMap() : base(30, "student_id")
            {
                
            }
        }
    }
}