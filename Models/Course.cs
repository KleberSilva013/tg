﻿using AfterClass.CoreModels;
using AfterClass.CoreModels.Entities;
using AfterClass.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AfterClass.Models
{
    public class Course : BaseNamedEntity
    {
        private Semester mSemester;

        public virtual Semester Semester
        {
            get
            {
                return mSemester;
            }
            set
            {
                mSemester = value;
            }
        }

        private IList<Discipline> mDisciplines;

        protected virtual IList<Discipline> Disciplines
        {
            get
            {
                return mDisciplines;
            }
            set
            {
                mDisciplines = value;
            }
        }

        public virtual Discipline[] GetDisciplines()
        {
            return mDisciplines.ToArray();
        }

        public virtual void AddDiscipline(Discipline discipline)
        {
            if (discipline == null)
                throw new NullDisciplineException("Impossível adicionar disciplina inválida");

            if (mDisciplines.Where(x => x.Id == discipline.Id).Any())
                throw new InvalidOperationException("O curso ja possui essa disciplina");

            mDisciplines.Add(discipline);
        }

        public virtual void RemoveDiscipline(Discipline discipline)
        {
            if (discipline == null)
                throw new NullDisciplineException("Impossível remover disciplina inválida");

            var disc = mDisciplines.Where(x => x.Id == discipline.Id).FirstOrDefault()
                ?? throw new InvalidOperationException("O curso não possui esta disciplina");

            mDisciplines.Remove(disc);
        }

        public class CourseMap : BaseNamedEntityMap<Course>
        {
            public CourseMap() : base(30, "course_id")
            {
                References(x => x.Semester)
                    .Not.Nullable();

                HasMany(x => x.Disciplines)
                    .Inverse()
                    .KeyColumn("course_id")
                    .LazyLoad();
            }
        }
    }
}