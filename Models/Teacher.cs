﻿using AfterClass.CoreModels.Entities;
using AfterClass.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace AfterClass.Models
{
    public class Teacher : BaseRegistrationEntity
    {
        private IList<Discipline> mDisciplines = new List<Discipline>();

        protected virtual IList<Discipline> Disciplines
        {
            get => mDisciplines;
            set { mDisciplines = value; }
        }

        public virtual Discipline[] GetDisciplines()
        {
            return mDisciplines.ToArray();
        }

        public virtual void AddDisciplines(Discipline discipline)
        {
            if (discipline == null)
                throw new NullDisciplineException("Disciplina não deve estar vazia.");

            if (discipline.Teacher != null)
                throw new DisciplineOwnerException("Disciplina ja possui um outro professor.");

            if (mDisciplines.Where(x => x.Id == discipline.Id).FirstOrDefault() != null)
                throw new DisciplineOwnerException("Professor ja possui esta disciplina.");

            discipline.Teacher = this;

            mDisciplines.Add(discipline);
        }

        public virtual void RemoveDiscipline(Discipline discipline)
        {
            if (discipline == null)
                throw new NullDisciplineException("Disciplina não deve estar vazia.");

            var disc = mDisciplines.Where(x => x.Id == discipline.Id).FirstOrDefault()
                ?? throw new DisciplineNotFoundException("Professor não possui a disciplina selecionada.");

            discipline.Teacher = null;

            mDisciplines.Remove(disc);
        }

        public virtual void UpdateDisciplines(Discipline[] disciplines)
        {
            var keep = disciplines.Where(x => mDisciplines.Where(y => x.Id != y.Id).FirstOrDefault() == null).ToArray();

            foreach(var d in mDisciplines)
            {
                bool ok = true;
                foreach(var kd in keep)
                {
                    if(d.Id == kd.Id)
                        ok = false;
                }

                if(!ok)
                    RemoveDiscipline(d);
            }

            foreach(var d in disciplines)
            {
                bool ok = true;
                foreach(var kd in keep)
                {
                    if(d.Id == kd.Id)
                        ok = false;
                }

                if(!ok)
                    AddDisciplines(d);
            }
        }

        public Teacher()
        {
            Role = CoreModels.Interfaces.RoleType.Teacher;
        }

        public class TeacherMap : BaseRegistrationEntityMap<Teacher>
        {
            public TeacherMap() : base(30, "teacher_id")
            {
                HasMany(x => x.Disciplines)
                    .Inverse()
                    .KeyColumn("course_id")
                    .LazyLoad()
                    .Cascade.SaveUpdate();
            }
        }
    }
}