﻿using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AfterClass.Database
{
    public abstract class BaseRepository<T> : IDisposable where T : class
    {
        private readonly IDataService mDataService;
        private ISession mSession;

        public BaseRepository(IDataService dataService)
        {
            mDataService = dataService ?? throw new ArgumentNullException(nameof(dataService));

            mSession = mDataService.OpenSession();
        }

        public BaseRepository(ISession session)
        {
            mSession = session;
        }

        public void Reload(object item)
        {
            mSession.Refresh(item);
        }

        public IDataService DataService
        {
            get
            {
                return mDataService;
            }
        }

        ~BaseRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                mSession.Dispose();
                mSession = null;
            }
        }

        public virtual T FindById(int id)
        {
            return mSession.Load<T>(id);
        }

        public virtual T FindByGuid(Guid id)
        {
            return mSession.Load<T>(id);
        }

        public virtual T FindByIdAndRefresh(int id)
        {
            T obj = mSession.Load<T>(id);
            mSession.Refresh(obj);

            return obj;
        }

        public virtual void Refresh(T entity)
        {
            mSession.Refresh(entity);
        }

        public virtual void Save(T entity)
        {
            mSession.SaveOrUpdate(entity);

            this.FlushSession();
        }

        public virtual void SaveWithoutFlushing(T entity)
        {
            mSession.SaveOrUpdate(entity);
        }

        public virtual T SaveOrUpdateCopy(T entity)
        {
            return (T)mSession.Merge(entity);
        }

        public virtual void Delete(T entity)
        {
            mSession.Delete(entity);
        }

        protected virtual void DeleteQuery(string query)
        {
            mSession.Delete(query);
        }

        protected ICriteria CreateCriteria<OTHER>() where OTHER : class
        {
            return mSession.CreateCriteria<OTHER>();
        }

        protected IQueryOver<T, T> QueryOver()
        {
            return mSession.QueryOver<T>();
        }

        protected IQueryOver<OTHER, OTHER> QueryOverGeneric<OTHER>() where OTHER : class
        {
            return mSession.QueryOver<OTHER>();
        }

        protected IQuery CreateQuery(string queryString)
        {
            return mSession.CreateQuery(queryString);
        }

        protected ISQLQuery CreateSQLQuery(string queryString)
        {
            return mSession.CreateSQLQuery(queryString);
        }

        protected System.Linq.IQueryable<T> Query()
        {
            return mSession.Query<T>();
        }

        public virtual IList<T> FindAll()
        {
            return mSession.CreateCriteria<T>().List<T>();
        }

        public virtual IList FindAllObjects()
        {
            return mSession.CreateCriteria<T>().List();
        }

        public int Count()
        {
            return (int)mSession.CreateCriteria<T>().SetProjection(NHibernate.Criterion.Projections.Count("id")).UniqueResult();
        }

        public abstract void DeleteAll();

        public void FlushSession()
        {
            mSession.Flush();
        }

        public void Refresh()
        {
            mSession.Clear();
        }

        public void Evict(T obj)
        {
            mSession.Evict(obj);
        }
    }
}
