﻿using System;
using NHibernate;
namespace AfterClass.Database
{
    public interface IDataService
    {
        string Host
        {
            get;
        }
        string Database
        {
            get;
        }
        string UserName
        {
            get;
        }
        string Password
        {
            get;
        }

        void CreateSchema();
        ISession OpenSession();
    }
}
