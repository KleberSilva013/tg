﻿using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Reflection;

namespace AfterClass.Database
{
    public class DataService : IDataService
    {
        private readonly Configuration mConfiguration;
        private readonly ISessionFactory mFactory;

        private Drivers.IDriver mDriver;

        public string Host
        {
            get; private set;
        }

        public string Database
        {
            get; private set;
        }

        public string UserName
        {
            get; private set;
        }

        public string Password
        {
            get; private set;
        }

        private void InitCommon(Drivers.IDriver driver, string host, string database, string userName, string password)
        {
            mDriver = driver ?? throw new ArgumentNullException(nameof(driver));

            NHibernate.Validator.Event.NHibernateSharedEngineProvider provider = new NHibernate.Validator.Event.NHibernateSharedEngineProvider();

            NHibernate.Validator.Cfg.Environment.SharedEngineProvider = provider;

            Host = host;
            Database = database;
            UserName = userName;
            Password = password;
        }

        public DataService(Assembly[] requiredAssemblies, Drivers.IDriver driver, string host, string database, string userName, string password, bool testing = false)
        {
            InitCommon(driver, host, database, userName, password);

            if(!testing)
            {
                mConfiguration = new Configuration();
                mConfiguration.Configure();
            }


#if DEBUG
            try
            {
#endif
                if(testing)
                {
                    mFactory = Fluently
                        .Configure()
                        .Database(mDriver.CreatePersistenConfigurer(host, database, userName, password))
                        .Mappings(m =>
                        {
                            foreach(Assembly a in requiredAssemblies)
                                m.FluentMappings.AddFromAssembly(a);
                        })
                        .ExposeConfiguration(BuildSchema)
                        .ExposeConfiguration(ConfigureEventHandlers)
                        .BuildSessionFactory();
                }
                else
                {
                    mFactory = Fluently.Configure(mConfiguration)
                       .Database(mDriver.CreatePersistenConfigurer(host, database, userName, password))
                       .Mappings(m =>
                       {
                           foreach(Assembly a in requiredAssemblies)
                               m.FluentMappings.AddFromAssembly(a);
                       })
                       .ExposeConfiguration(ConfigureEventHandlers)
                       .BuildSessionFactory();
                }

#if DEBUG
            }
            catch(Exception ex)
            {
                throw ex;
            }
#endif
        }

        private static void BuildSchema(Configuration cfg)
        {
            SchemaExport sc = new SchemaExport(cfg);
            sc.Execute(true, true, false);
        }

        private static void ConfigureEventHandlers(Configuration cfg)
        {
            cfg.EventListeners.PreUpdateEventListeners = new NHibernate.Event.IPreUpdateEventListener[] { new NHibernate.Validator.Event.ValidatePreUpdateEventListener() };
            cfg.EventListeners.PreInsertEventListeners = new NHibernate.Event.IPreInsertEventListener[] { new NHibernate.Validator.Event.ValidatePreInsertEventListener() };
        }

        public void CreateSchema()
        {
            System.Diagnostics.Debug.Assert(mConfiguration != null);

            BuildSchema(mConfiguration);
        }

        public ISession OpenSession()
        {
            ISession session = mFactory.OpenSession();
            session.FlushMode = NHibernate.FlushMode.Commit;

            return session;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if(!disposedValue)
            {
                if(disposing)
                {

                }
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
