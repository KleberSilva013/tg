﻿using FluentNHibernate.Cfg.Db;

namespace AfterClass.Database.Drivers
{
    public interface IDriver
    {
        string BuildConnectionString(string host, string database, string userName, string password);

        IPersistenceConfigurer CreatePersistenConfigurer(string host, string database, string userName, string password);
    }
}
