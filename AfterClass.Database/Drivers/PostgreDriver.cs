﻿using System;
using FluentNHibernate.Cfg.Db;

namespace AfterClass.Database.Drivers
{
    public class PostgreDriver : IDriver
    {
        public string BuildConnectionString(string host, string database, string userName, string password)
        {
            if(string.IsNullOrWhiteSpace(host))
                throw new ArgumentException(nameof(host));

            if(string.IsNullOrWhiteSpace(database))
                throw new ArgumentException(nameof(database));

            if(string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException(nameof(userName));

            if(string.IsNullOrWhiteSpace(password))
                throw new ArgumentException(nameof(password));

            return "Server=" + host + ";Database=" + database + ";User ID=" + userName + ";Password=" + password + ";";
        }

        public IPersistenceConfigurer CreatePersistenConfigurer(string host, string database, string userName, string password)
        {
            var connection = this.BuildConnectionString(host, database, userName, password);
            return FluentNHibernate.Cfg.Db.PostgreSQLConfiguration.PostgreSQL82.ConnectionString(connection);
        }
    }
}
