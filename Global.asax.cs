using AfterClass.CoreModels.Interfaces;
using AfterClass.Database;
using AfterClass.Repositories;
using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace AfterClass
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IDataService DataService
        {
            get; private set;
        }

        private Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("AfterClass")
            };
        }

        protected void Application_Start()
        {
            this.Error += MvcApplication_Error;

            DataService = new DataService(GetAssemblies(),
                new AfterClass.Database.Drivers.PostgreDriver(),
                "localhost", "afterclass", "manager", "manager");

            //We keep this here because, idk exactly what wrong can happen in this piece of shit

            //MvcApplication.DataService.CreateSchema();
            //var repo = new TeacherRepository(DataService);
            //var admin = new Teacher()
            //{
            //    Name = "admin",
            //    UserName = "admin",
            //    Password = "a",
            //    Role = RoleType.Administrator,
            //    Email = "email@email.com"
            //};
            //repo.Save(admin);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }

        private void MvcApplication_Error(object sender, EventArgs e)
        {
            Controllers.ExceptionController.TrapedException = this.Server.GetLastError();
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    string userName = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                    string roles;
                    using (var shared = MvcApplication.DataService.OpenSession())
                    {
                        var studentRepo = new StudentRepository(shared);
                        IUserEntity user = studentRepo.FindByUserName(userName);

                        if (user == null)
                        {
                            var teacherRepo = new TeacherRepository(shared);
                            user = teacherRepo.FindByUserName(userName);
                        }

                        if (user == null)
                            throw new Exceptions.PostAuthenticateRequestFailException("Falha ao validar dados obtidos pelos cookies");

                        roles = user.Role.ToString();

                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(userName, "Forms"), new string[] { roles });
                    }
                }
            }
        }
    }
}
