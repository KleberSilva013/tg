﻿using AfterClass.Models;
using System.Collections.Generic;

namespace AfterClass.ViewModels
{
    public class TeacherDisciplinesViewModel
    {
        public IList<Discipline> Disciplines
        {
            get; set;
        }

        public TeacherDisciplinesViewModel(IList<Discipline> disciplines)
        {
            this.Disciplines = disciplines;
        }
    }
}