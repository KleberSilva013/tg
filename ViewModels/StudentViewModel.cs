﻿using AfterClass.CoreModels.Interfaces;
using AfterClass.CoreModels.ViewModels;
using AfterClass.Exceptions;
using AfterClass.Models;

namespace AfterClass.ViewModels
{
    public class StudentViewModel : BaseRegistrationViewModel
    {
        public StudentViewModel()
        {
            //used through reflection by asp
        }

        public StudentViewModel(Student student)
        {
            if(student == null)
                throw new NullStudentException("Nenhuma disciplina foi adicionada");
            
            Id = student.Id;
            Name = student.Name;
            UserName = student.UserName;
            Password = student.Password;
            Email = student.Email;
            Role = RoleType.Student;
        }
    }
}