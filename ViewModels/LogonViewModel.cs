﻿using AfterClass.CoreModels.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace AfterClass.ViewModels
{
    public class LogonViewModel
    {
        public LogonViewModel()
        {

        }

        [Display(Name = "Nome de Usuário")]
        [Required(ErrorMessage = "Informe um nome de usuário válido", AllowEmptyStrings = false)]
        public string UserName
        {
            get; set;
        }

        [Display(Name = "Senha")]
        [Required(ErrorMessage = "Informe uma senha válida", AllowEmptyStrings = false)]
        public string Password
        {
            get; set;
        }

        public static IUserEntity LoggedUser
        {
            get; set;
        }

        public static void Logoff()
        {
            if (LogonViewModel.LoggedUser == null)
                throw new InvalidOperationException(nameof(LoggedUser));

            LogonViewModel.LoggedUser = null;
        }
    }
}