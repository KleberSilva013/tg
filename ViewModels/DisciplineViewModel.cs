﻿using AfterClass.CoreModels.ViewModels;
using AfterClass.Exceptions;
using AfterClass.Models;
using System.Collections.Generic;
using System.Linq;

namespace AfterClass.ViewModels
{
    public class DisciplineViewModel : BaseNamedViewModel
    {
        public virtual Teacher Teacher
        {
            set;get;
        }

        private IList<Student> mStudents = new List<Student>();

        protected virtual IList<Student> Students
        {
            get
            {
                return mStudents;
            }
            set
            {
                mStudents = value;
            }
        }

        public virtual Student[] GetStudents()
        {
            return mStudents.ToArray();
        }

        public virtual void AddStudent(Student student)
        {
            if(student == null)
                throw new NullStudentException("Impossível adicionar aluno inválido");

            if(mStudents.Where(x => x.Id == student.Id).Any())
                throw new System.InvalidOperationException("O aluno ja faz parte desta discplina");

            mStudents.Add(student);
        }

        public virtual void RemoveStudent(Student student)
        {
            if(student == null)
                throw new NullStudentException("Impossível remover aluno inválido");

            var stud = mStudents.Where(x => x.Id == student.Id).FirstOrDefault()
                ?? throw new System.InvalidOperationException("O aluno não faz parte desta disciplina");

            mStudents.Remove(stud);
        }

        public DisciplineViewModel()
        {

        }

        public DisciplineViewModel(Discipline discipline)
        {
            if(discipline == null)
                throw new NullDisciplineException("Nenhuma disciplina foi selecionada");

            this.Id = discipline.Id;
            this.Name = discipline.Name;
            this.Teacher = discipline.Teacher;
            this.Students = discipline.GetStudents();
        }
    }
}