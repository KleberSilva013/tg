﻿using AfterClass.CoreModels.ViewModels;
using AfterClass.Exceptions;
using AfterClass.Models;
using System.Collections.Generic;
using System.Linq;

namespace AfterClass.ViewModels
{
    public class TeacherViewModel : BaseRegistrationViewModel
    {
        private IList<Discipline> mDisciplines = new List<Discipline>();
        private readonly Teacher mTeacher;

        public IList<Discipline> Disciplines
        {
            get => mDisciplines;
            set
            {
                mDisciplines = value;
            }
        }

        public Discipline[] GetDisciplines()
        {
            return mDisciplines.ToArray();
        }

        public void AddDisciplines(Discipline discipline)
        {
            if (mDisciplines.Select(x => x.Id == discipline.Id).Any())
                throw new System.ArgumentException("Este professor ja possui a disciplina: " + discipline.Name.ToUpper());

            discipline.Teacher = mTeacher;

            mDisciplines.Add(discipline);
        }

        public void RemoveDiscipline(Discipline discipline)
        {
            var disc = mDisciplines.Where(x => x.Id == discipline.Id).FirstOrDefault()
                ?? throw new System.ArgumentException("Professor não possui esta disciplina");

            discipline.Teacher = null;

            mDisciplines.Remove(disc);
        }

        public TeacherViewModel()
        {

        }

        public TeacherViewModel(Teacher teacher)
        {
            this.mTeacher = teacher ?? throw new NullTeacherException("Nenhum professor foi selecionado.");
            this.Id = teacher.Id;
            this.Name = teacher.Name == null ? "" : teacher.Name;
            this.UserName = teacher.UserName == null ? "" : teacher.UserName;
            this.Password = teacher.Password == null ? "" : teacher.Password;
            this.Email = teacher.Email == null ? "" : teacher.Email;
            this.Role = CoreModels.Interfaces.RoleType.Teacher;
            this.Disciplines = teacher.GetDisciplines();
        }
    }
}