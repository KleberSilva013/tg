﻿using System.Web;
using System.Web.Mvc;

namespace AfterClass
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute()
            {
                View = "~/Exception/ExceptionHandler.cshtml"
            });
        }
    }
}
