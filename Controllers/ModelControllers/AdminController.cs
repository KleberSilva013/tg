﻿using AfterClass.CoreModels.Interfaces;
using AfterClass.Models;
using AfterClass.Repositories;
using AfterClass.ViewModels;
using NHibernate;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AfterClass.Controllers.ModelControllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        #region Repositories

        private static ISession Shared = MvcApplication.DataService.OpenSession();
        private readonly StudentRepository mStudentRepo;
        private readonly TeacherRepository mTeacherRepo;
        private readonly DisciplineRepository mDisciRepo;
        private readonly CourseRepository mCourseRepo;
        private readonly SemesterRepository mSemRepo;

        #endregion

        public AdminController()
        {
            mStudentRepo = new StudentRepository(Shared);
            mTeacherRepo = new TeacherRepository(Shared);
            mDisciRepo = new DisciplineRepository(Shared);
            mCourseRepo = new CourseRepository(Shared);
            mSemRepo = new SemesterRepository(Shared);
        }

        private static object mViewModel;

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ConfigureDatabase()
        {
            MvcApplication.DataService.CreateSchema();

            var admin = new Teacher()
            {
                Name = "admin",
                UserName = "admin",
                Password = "a",
                Role = RoleType.Administrator,
                Email = "email@email.com"
            };
            mTeacherRepo.Save(admin);

            return RedirectToAction("Index", "Admin");
        }

        #region Student
        public ActionResult ListAllStudents()
        {
            using(var repo = new StudentRepository(MvcApplication.DataService.OpenSession()))
            {
                return View(repo.FindAll());
            }
        }

        public ActionResult AddStudent()
        {
            return View(new Student());
        }

        public ActionResult EditStudent(int id)
        {
            using(var repo = new StudentRepository(MvcApplication.DataService.OpenSession()))
            {
                var student = repo.FindById(id);
                var viewModel = new StudentViewModel(student);

                return View(viewModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditStudent(StudentViewModel model)
        {
            using(var session = MvcApplication.DataService.OpenSession())
            using(var transaction = session.BeginTransaction())
            {
                var sRepo = new StudentRepository(session);
                if(sRepo.IsUserNameValid(model.UserName, model.Id) &&
                    mTeacherRepo.IsUserNameValid(model.UserName))
                {
                    if(ModelState.IsValid)
                    {
                        var student = sRepo.FindById(model.Id);
                        student.Name = model.Name;
                        student.UserName = model.UserName;
                        student.Password = model.Password;
                        student.Email = model.Email;

                        sRepo.Save(student);
                        transaction.Commit();


                        return RedirectToAction("ListAllStudents", "Admin");
                    }
                    ModelState.AddModelError("", "Falha ao salvar aluno.");
                    return View(model);
                }

                ModelState.AddModelError("", "Nome de usuário ja está em uso.");
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddStudent(Student model)
        {
            using(var session = MvcApplication.DataService.OpenSession())
            using(var trx = session.BeginTransaction())
            {
                var repo = new StudentRepository(session);
                if(repo.IsUserNameValid(model.UserName) &&
                mTeacherRepo.IsUserNameValid(model.UserName))
                {
                    if(ModelState.IsValid)
                    {
                        repo.Save(model);
                        trx.Commit();

                        return RedirectToAction("ListAllStudents", "Admin");
                    }

                    ModelState.AddModelError("", "Falha ao salvar aluno.");
                    return View(model);
                }

                ModelState.AddModelError("", "Nome de usuário já está em uso.");
                return View(model);
            }
        }

        public ActionResult DeleteStudent(int id)
        {
            using(var session = MvcApplication.DataService.OpenSession())
            using(var trx = session.BeginTransaction())
            {
                var repo = new StudentRepository(session);
                repo.Delete(repo.FindById(id));
                trx.Commit();

                return RedirectToAction("ListAllStudents", "Admin");
            }
        }
        #endregion

        #region Teacher

        public ActionResult ListAllTeachers()
        {
            using(var repo = new TeacherRepository(MvcApplication.DataService.OpenSession()))
            {
                return View(repo.FindAll().Where(x => x.Role == RoleType.Teacher).ToList());
            }
        }

        public ActionResult AddTeacher()
        {
            var teacher = new Teacher();

            return View(teacher);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTeacher(Teacher model)
        {
            using(var session = MvcApplication.DataService.OpenSession())
            using(var trx = session.BeginTransaction())
            {
                var tRepo = new TeacherRepository(session);
                var sRepo = new StudentRepository(session);
                if(sRepo.IsUserNameValid(model.UserName) &&
                    tRepo.IsUserNameValid(model.UserName))
                {
                    if(ModelState.IsValid)
                    {
                        tRepo.Save(model);
                        trx.Commit();

                        return RedirectToAction("ListAllTeachers", "Admin");
                    }

                    ModelState.AddModelError("", "Falha ao salvar aluno.");
                    return View(model);
                }

                ModelState.AddModelError("", "Nome de usuário já está em uso.");
                return View(model);
            }
        }

        public ActionResult EditTeacher(int id)
        {
            var teacher = mTeacherRepo.FindById(id);
            var viewModel = new TeacherViewModel(teacher);

            mViewModel = viewModel;

            return View(viewModel);
        }

        private void FlushAll()
        {
            mCourseRepo.FlushSession();
            mDisciRepo.FlushSession();
            mTeacherRepo.FlushSession();
            mStudentRepo.FlushSession();
            mSemRepo.FlushSession();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTeacher(TeacherViewModel model)
        {
            return SaveOrUpdateTeacher(model);
        }

        private ActionResult SaveOrUpdateTeacher(TeacherViewModel model)
        {
            if(mStudentRepo.IsUserNameValid(model.UserName) &&
                mTeacherRepo.IsUserNameValid(model.UserName, model.Id))
            {
                if(ModelState.IsValid)
                {
                    var teacher = mTeacherRepo.FindById(model.Id);
                    teacher.Name = model.Name;
                    teacher.UserName = model.UserName;
                    teacher.Password = model.Password;
                    teacher.Email = model.Email;
                    teacher.UpdateDisciplines(model.GetDisciplines());

                    mTeacherRepo.Save(teacher);

                    return RedirectToAction("ListAllTeachers", "Admin");
                }
                ModelState.AddModelError("", "Falha ao salvar professor.");
                return View(model);
            }

            ModelState.AddModelError("", "Nome de usuário ja está em uso.");
            return View(model);
        }

        private IList<Discipline> AvailableDisciplines(IEnumerable<Discipline> disciplines)
        {
            return mDisciRepo.FindAll()
               .Where(x => disciplines
               .Where(y => x.Id != y.Id && x.Teacher == null).FirstOrDefault() == null)
               .ToList();
        }

        public ActionResult TeacherDisciplines()
        {
            var viewModel = mViewModel as TeacherViewModel;
            var tdViewModel = new TeacherDisciplinesViewModel(viewModel.GetDisciplines());

            ViewBag.TeacherName = viewModel.Name;

            return View(tdViewModel.Disciplines);
        }

        [HttpPost]
        public ViewResult TeacherDisciplines(IEnumerable<string> selectedDiscipline)
        {
            var viewModel = mViewModel as TeacherViewModel;
            var newModel = new TeacherViewModel()
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                UserName = viewModel.UserName,
                Email = viewModel.Email,
                Password = viewModel.Password,
                Role = viewModel.Role
            };

            var list = new List<Discipline>();
            foreach(var d in viewModel.Disciplines)
                list.Add(d);

            foreach(var s in selectedDiscipline)
                list.Add(mDisciRepo.FindById(int.Parse(s)));

            newModel.Disciplines = list;

            mViewModel = newModel;
            ViewBag.TeacherName = newModel.Name;

            return View("TeacherDisciplines", newModel.Disciplines);
        }

        public ViewResult ConfirmTeacher()
        {
            return View("EditTeacher", mViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmTeacher(TeacherViewModel model)
        {
            return SaveOrUpdateTeacher(mViewModel as TeacherViewModel);
        }

        public ActionResult DeleteTeacher(int id)
        {
            using(var session = MvcApplication.DataService.OpenSession())
            using(var trx = session.BeginTransaction())
            {
                var repo = new TeacherRepository(session);
                repo.Delete(repo.FindById(id));
                trx.Commit();

                return RedirectToAction("ListAllTeachers", "Admin");
            }
        }

        public ActionResult TeacherRemoveDiscipline(int id)
        {
            var viewModel = (mViewModel as TeacherViewModel);

            var list = new List<Discipline>();
            foreach(var d in viewModel.GetDisciplines())
            {
                if(d.Id != id)
                    list.Add(d);
            }
            viewModel.Disciplines = list;
            mViewModel = viewModel;

            return RedirectToAction("TeacherDisciplines", "Admin");
        }

        #endregion

        #region Discipline

        public ActionResult ListAllDisciplines()
        {
            return View(mDisciRepo.FindAll());
        }

        public ActionResult AddDiscipline()
        {
            return View(new Discipline());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDiscipline(Discipline model)
        {
            if(ModelState.IsValid)
            {
                mDisciRepo.Save(model);
                return RedirectToAction("ListAllDisciplines", "Admin");
            }

            ModelState.AddModelError("", "Falha ao salvar disciplina");
            return View(model);
        }

        public ActionResult EditDiscipline(int id)
        {
            var discipline = mDisciRepo.FindById(id);
            var viewModel = new DisciplineViewModel(discipline);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDiscipline(DisciplineViewModel model)
        {
            if(ModelState.IsValid)
            {
                var discipline = mDisciRepo.FindById(model.Id);
                discipline.Name = model.Name;
                discipline.Teacher = model.Teacher;
                discipline.UpdateStudents(model.GetStudents());

                mDisciRepo.Save(discipline);
                return RedirectToAction("ListAllDisciplines", "Admin");
            }

            ModelState.AddModelError("", "Falha ao salvar disciplina");
            return View();
        }

        public ActionResult DisciplineView()
        {
            return View();
        }

        public ActionResult DeleteDiscipline(int id)
        {
            var discipline = mDisciRepo.FindById(id);
            if(discipline.Teacher != null)
            {
                var teacher = mTeacherRepo.FindById(discipline.Teacher.Id);
                teacher.RemoveDiscipline(discipline);
                mTeacherRepo.Save(teacher);
            }
            mDisciRepo.Delete(discipline);
            mDisciRepo.FlushSession();
            return RedirectToAction("ListAllDisciplines", "Admin");
        }

        #endregion

        #region Course

        public ActionResult ListAllCourses()
        {
            return View(mCourseRepo.FindAll());
        }

        public ActionResult AddCourse()
        {
            return View(new Course());
        }

        #endregion

        #region Semester

        public ActionResult ListAllSemesters()
        {
            return View(mSemRepo.FindAll());
        }

        public ActionResult AddSemester()
        {
            return View(new Semester());
        }

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            ExceptionController.TrapedException = filterContext.Exception;
            filterContext.Result = RedirectToAction("ExceptionHandler", "Exception");
        }
    }
}