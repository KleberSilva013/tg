﻿using AfterClass.CoreModels.Interfaces;
using AfterClass.Repositories;
using AfterClass.ViewModels;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace AfterClass.Controllers.AuthenticationControllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LogonViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userName = model.UserName;
                var password = model.Password;

                using (var session = MvcApplication.DataService.OpenSession())
                {
                    var sRepo = new StudentRepository(session);
                    IUserEntity user = sRepo.DoLogin(userName, password);

                    if (user == null)
                    {
                        var teacherRepo = new TeacherRepository(session);
                        user = teacherRepo.DoLogin(userName, password);
                    }

                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(userName, false);
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1
                            && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//")
                            && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }

                        switch (user.Role)
                        {
                            case (RoleType.Administrator):
                                return RedirectToAction("Index", "Admin");
                            case (RoleType.Student):
                                return RedirectToAction("Index", "Student");
                            case (RoleType.Teacher):
                                return RedirectToAction("Index", "Teacher");
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Nome de usuário ou senha incorretos");
                    }
                }
            }

            return View(model);
        }

        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            ExceptionController.TrapedException = filterContext.Exception;
            filterContext.Result = RedirectToAction("ExceptionHandler", "Exception");
        }
    }
}