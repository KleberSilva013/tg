﻿using System;
using System.Web.Mvc;

namespace AfterClass.Controllers
{
    [AllowAnonymous]
    [HandleError(ExceptionType = typeof(Exception))]
    public class ExceptionController : Controller
    {
        // GET: Exception

        public static Exception TrapedException
        {
            set; get;
        }

        public ActionResult ExceptionHandler()
        {
            return View(TrapedException);
        }
    }
}