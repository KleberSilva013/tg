﻿using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class PostAuthenticateRequestFailException : System.Exception
    {
        public PostAuthenticateRequestFailException(string message) : base(message)
        {

        }

        public PostAuthenticateRequestFailException(string message, System.Exception innerException)
            : base(message, innerException)
        {

        }

        public PostAuthenticateRequestFailException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }
}