﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class NullStudentException : Exception
    {
        public NullStudentException(string message) : base(message)
        {

        }

        public NullStudentException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public NullStudentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}