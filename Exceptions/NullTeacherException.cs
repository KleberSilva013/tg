﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class NullTeacherException : Exception
    {
        public NullTeacherException(string message) : base(message)
        {

        }

        public NullTeacherException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        public NullTeacherException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}