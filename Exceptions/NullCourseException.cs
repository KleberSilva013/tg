﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class NullCourseException : Exception
    {
        public NullCourseException(string message) : base(message)
        {

        }

        public NullCourseException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public NullCourseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}