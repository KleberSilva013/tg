﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class DisciplineOwnerException : Exception
    {
        public DisciplineOwnerException(string message) : base(message)
        {

        }

        public DisciplineOwnerException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public DisciplineOwnerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}