﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class InvalidYearArgumentException : Exception
    {
        public InvalidYearArgumentException(string message) : base(message)
        {

        }

        public InvalidYearArgumentException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public InvalidYearArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}