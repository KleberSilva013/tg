﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class DisciplineNotFoundException : Exception
    {
        public DisciplineNotFoundException(string message) : base(message)
        {

        }

        public DisciplineNotFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public DisciplineNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}