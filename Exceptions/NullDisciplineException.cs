﻿using System;
using System.Runtime.Serialization;

namespace AfterClass.Exceptions
{
    public class NullDisciplineException : Exception
    {
        public NullDisciplineException(string message) : base(message)
        {

        }

        public NullDisciplineException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public NullDisciplineException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}