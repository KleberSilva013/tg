﻿using AfterClass.Database;
using AfterClass.Models;
using NHibernate;

namespace AfterClass.Repositories
{
    public class SemesterRepository : BaseRepository<Semester>
    {
        public SemesterRepository(IDataService service) : base(service)
        {

        }

        public SemesterRepository(ISession session) : base(session)
        {

        }

        public override void DeleteAll()
        {
            this.CreateQuery("from Semester");
        }
    }
}