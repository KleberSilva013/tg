﻿using AfterClass.Database;
using AfterClass.Models;
using NHibernate;

namespace AfterClass.Repositories
{
    public class DisciplineRepository : BaseRepository<Discipline>
    {
        public DisciplineRepository(IDataService service) : base(service)
        {

        }

        public DisciplineRepository(ISession session) : base(session)
        {

        }

        public override void DeleteAll()
        {
            this.CreateQuery("from Discipline");
        }
    }
}