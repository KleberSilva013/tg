﻿using AfterClass.Database;
using AfterClass.Models;
using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace AfterClass.Repositories
{
    public class StudentRepository : BaseRepository<Student>
    {
        public StudentRepository(IDataService service) : base(service)
        {

        }

        public StudentRepository(ISession session) : base(session)
        {

        }

        public override void DeleteAll()
        {
            this.CreateQuery("from Student");
        }

        public Student FindByUserName(string userName)
        {
            return this.CreateQuery(string.Format("select s from Student s where s.UserName = '{0}'", userName))
                .UniqueResult() as Student;
        }

        public bool IsUserNameValid(string userName, int id = -1)
        {
            return this.CreateCriteria<Student>()
                .Add(Restrictions.Where<Student>(x =>
                x.UserName == userName && x.Id != id))
                .UniqueResult() == null;
        }

        public Student DoLogin(string userName, string password)
        {
            return this.CreateQuery(string.Format("select s from Student s where s.UserName = '{0}' " +
                "and s.Password = '{1}'", userName, password))
                .UniqueResult() as Student;
        }
    }
}