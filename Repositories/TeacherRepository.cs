﻿using AfterClass.Database;
using AfterClass.Models;
using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace AfterClass.Repositories
{
    public class TeacherRepository : BaseRepository<Teacher>
    {
        public TeacherRepository(IDataService service) : base(service)
        {

        }

        public TeacherRepository(ISession session) : base(session)
        {

        }

        public override void DeleteAll()
        {
            this.CreateQuery("from Teacher");
        }

        public Teacher FindByUserName(string value)
        {
            return this.CreateQuery(string.Format("select t from Teacher t where t.UserName = '{0}'", value))
                .UniqueResult() as Teacher;
        }

        public bool IsUserNameValid(string userName, int id = -1)
        {
            return this.CreateCriteria<Teacher>()
                .Add(Restrictions.Where<Teacher>(x => 
                x.UserName == userName && x.Id != id))
                .UniqueResult() == null;
        }

        public Teacher DoLogin(string userName, string password)
        {
            return this.CreateQuery(string.Format("select t from Teacher t where t.UserName = '{0}' " +
                "and t.Password = '{1}'", userName, password))
                .UniqueResult() as Teacher;
        }
    }
}