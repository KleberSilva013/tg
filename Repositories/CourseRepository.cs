﻿using AfterClass.Database;
using AfterClass.Models;
using NHibernate;

namespace AfterClass.Repositories
{
    public class CourseRepository : BaseRepository<Course>
    {
        public CourseRepository(IDataService service) : base(service)
        {

        }

        public CourseRepository(ISession session) : base(session)
        {

        }

        public override void DeleteAll()
        {
            this.CreateQuery("from Course");
        }
    }
}